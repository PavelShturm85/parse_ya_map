
import threading
from pymongo import MongoClient
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


settings = {
    "search_word": "Бетон",
    "latitude_begin": 69.50,
    "latitude_end": 39.50,
    "longitude_begin": 30.30,
    "longitude_end": 180.00,
    "company_list": "link-wrapper",
    "chrome_driver": "./chromedriver",
    "btn_all_detail": "orgpage-info-view__anchor",
    "not_company_list": "nothing-found-view__text",
    "address_in_list": "search-business-snippet-view__address",
    "title_full_list": "orgpage-header-view__header",
    "url_full_list": "orgpage-url-view__url",
    "address_full_list": "orgpage-contacts-view__address",
    "phone_full_list": "orgpage-contacts-view__phone",
    "title_short_list": "org__name",
    "url_short_list": ".link.link__control.i-bem.link_js_inited[itemprop=url]",
    "address_short_list": "org__address-name",
    "phone_short_list": "org__phone-number",

}

NO_SAVE_LIST = ('Узбекистан', 'Казахстан', 'Украина', 'Беларусь', 'Турция', 'Грузия', 'Азербайджан', 'Армения',
                'Киргизия', 'Туркменистан', 'Таджикистан', 'Китай', 'Монголия', 'Япония', 'Латвия', 'Литва',
                'Эстония', 'Финляндия', 'Норвегия', 'Молдова', 'Україна')


class ParseYaMap():
    def __init__(self, search_word=settings["search_word"]):
        self.template_url = 'https://yandex.ru/maps/?ll={longitude}%2C{latitude}&mode=search&sll={longitude}%2C{latitude}&text={search_word}&z=13.22'
        self.search_word = search_word
        mongo = MongoClient('127.0.0.1', 27017, connect=False)
        db = mongo.companies_db
        self.company = db.ya_companies
        self.lock = threading.Lock()
        options = webdriver.ChromeOptions()
        #options.add_argument('--headless')
        #options.add_argument('--disable-gpu')
        chrome = webdriver.Chrome(executable_path=settings['chrome_driver'], chrome_options=options)
        chrome.implicitly_wait(1)
        chrome.set_window_size(1920, 1080)
        chrome.maximize_window()
        self.chrome = chrome
        

    def __check_company_by_no_save_list(self, address_company):
        check = True
        for no_save_country in NO_SAVE_LIST:
            if no_save_country in address_company:
                check = False
        return check

    def __save_company(self, companies):

        for curr_company in companies:
            address_company = curr_company.get("address_short")
            if self.__check_company_by_no_save_list(address_company):
                with self.lock:
                    self.company.save(curr_company)
                    print("save: {}".format(curr_company))

    def __get_company_list(self, url):
        self.chrome.get(url)
        company_list = self.chrome.find_elements_by_class_name(
            settings['company_list'])
        len_after_scroll = 0
        len_before_scroll = len(company_list)

        while len_after_scroll != len_before_scroll:
            for i, com in enumerate(company_list):
                if (i+1) == len(company_list):
                    len_before_scroll = len(company_list)
                    ActionChains(self.chrome).move_to_element(
                        com).pause(1).perform()
                    company_list = self.chrome.find_elements_by_class_name(
                        settings['company_list'])
                    len_after_scroll = len(company_list)
        print(len_after_scroll, len_before_scroll)
        urls = [url.get_attribute('href') for url in company_list]
        return urls
    
    def __get_company_details(self, company_list):
        for link in company_list:
            print(link)
            with self.lock:
                is_in_base = self.company.find_one({"ya_link": link})
            if is_in_base:
                print("Организация уже в базе.")
                continue
            self.chrome.get(link)
            try:
                btn_all_detail = self.chrome.find_element_by_class_name(
                    settings["btn_all_detail"])
                ActionChains(self.chrome).move_to_element(
                    btn_all_detail).click().perform()
                title = self.chrome.find_element_by_class_name(
                    settings["title_full_list"]).text
                try:
                    urls = self.chrome.find_elements_by_class_name(
                        settings["url_full_list"])
                    url_list = list(
                        set([url.get_attribute('href') for url in urls]))
                except:
                    url_list = []
                try:
                    address = self.chrome.find_element_by_class_name(
                        settings["address_full_list"])
                    try:
                        address_full = address.get_attribute("title")
                    except:
                        address_full = ''
                    try:
                        address_short = address.text
                    except:
                        address_short = ''
                except:
                    address_full = ''
                    address_short = ''
                try:
                    phones = self.chrome.find_elements_by_class_name(
                        settings["phone_full_list"])
                    phone_list = [phone.text for phone in phones]
                except:
                    phone_list = []
                company_details = {
                    "title": title,
                    "url": url_list,
                    "address": address_full,
                    "address_short": address_short,
                    "phone": phone_list,
                    "ya_link": link,
                }
                yield company_details

            except:
                title = self.chrome.find_element_by_class_name(
                    settings["title_short_list"]).text
                try:
                    urls = self.chrome.find_elements_by_css_selector(
                        settings["url_short_list"])
                    url_list = list(
                        set([url.get_attribute('href') for url in urls]))
                except:
                    url_list = []
                try:
                    phones = self.chrome.find_element_by_class_name(
                        settings["phone_short_list"]).text
                    phone_list = [phones, ]
                except:
                    phone_list = []
                try:
                    address = self.chrome.find_element_by_class_name(
                        settings["address_short_list"])
                    try:
                        address_full = address.get_attribute("title")
                    except:
                        address_full = ''
                    try:
                        address_short = address.text
                    except:
                        address_short = ''
                except:
                    address_full = ''
                    address_short = ''
                company_details = {
                    "title": title,
                    "url": url_list,
                    "address": address_full,
                    "address_short": address_short,
                    "phone": phone_list,
                    "ya_link": link,
                }
                yield company_details
            

    def _check_page_by_company(self, num, latitude_begin=77.00, latitude_end=41.00, longitude_begin=27.00, longitude_end=180.00):
        def next_step(latitude, longitude):
            if longitude <= longitude_end:
                longitude += 0.19
            elif longitude >= longitude_end:
                longitude = longitude_begin
                if latitude > 65.0:
                    latitude -= 0.04
                elif 60.0 < latitude <= 65.0:
                    latitude -= 0.05
                elif 55.0 < latitude <= 60.0:
                    latitude -= 0.06
                elif 50.0 < latitude <= 55.0:
                    latitude -= 0.07
                elif 45.0 < latitude <= 50.0:
                    latitude -= 0.08
                elif 40.0 < latitude <= 45.0:
                    latitude -= 0.09
                else:
                    latitude -= 0.1
            return latitude, longitude

        latitude, longitude = (latitude_begin, longitude_begin)
        with self.lock:
            is_last_point = self.company.find(
                {"last_point_{}".format(num): True})
        if is_last_point:
            for point in is_last_point:
                latitude, longitude = point.get("coordinate")
        if longitude > longitude_end:
            latitude, longitude = next_step(latitude, longitude)
        if latitude <= latitude_end:
            print('Поток-{}: Сбор информации окончен'.format(num))
        while latitude >= latitude_end:
            query = {"last_point_{}".format(num): True}
            data = {"last_point_{}".format(num): True, "coordinate": [
                latitude, longitude]}
            with self.lock:
                self.company.update(query, data, upsert=True)
            url = self.template_url.format(longitude=str(longitude), latitude=str(
                latitude), search_word=str(self.search_word))
            print("Поток-{}: {}".format(num, url))
            
            self.chrome.get(url)
            if not self.chrome.find_elements_by_class_name(settings['not_company_list']):
                print("Поток-{}: нашли элементы".format(num))
                company_list = self.__get_company_list(url)
                print("Поток-{}: получили список".format(num))
                self.__save_company(self.__get_company_details(company_list))

                latitude, longitude = next_step(latitude, longitude)
                if latitude <= latitude_end:
                    print('Поток-{}: Сбор информации окончен'.format(num))
                    self.chrome.quit()
            else:
                latitude, longitude = next_step(latitude, longitude)
                print("Поток-{}: нет данных по запросу '{}' на карте.".format(num,
                    self.search_word))
                if latitude <= latitude_end:
                    print('Поток-{}: Сбор информации окончен'.format(num))
                    self.chrome.quit()
                continue


def __select_number_threads():
    import multiprocessing
    cpu_count = int(multiprocessing.cpu_count())
    if cpu_count > 4:
        threads = cpu_count - 2
    elif 1 < cpu_count <= 4:
        threads = cpu_count - 1
    else:
        threads = cpu_count
    return threads


def get_targets_and_params():
    step = __select_number_threads()
    latitude_begin = settings["latitude_begin"]
    latitude_end = settings["latitude_end"]
    len_latitude = latitude_begin - latitude_end
    step_for_thread = len_latitude/step
    params = []
    target = []
    curr_latitude_begin = latitude_begin
    curr_latitude_end = latitude_begin - step_for_thread
    num = 1
    
    while curr_latitude_begin - step_for_thread > latitude_end and curr_latitude_end > latitude_end:
        ya_parser = ParseYaMap()
        params.append((num, curr_latitude_begin, curr_latitude_end,
                       settings["longitude_begin"], settings["longitude_end"],))
        target.append(ya_parser._check_page_by_company)
        num += 1
        curr_latitude_begin = curr_latitude_end
        curr_latitude_end = curr_latitude_begin - step_for_thread
    return zip(target, params)


def main():
    targets_and_params = get_targets_and_params()

    for target, param in targets_and_params:
        print(param)
        my_thread = threading.Thread(target=target, args=(*param,))
        my_thread.start()


if __name__ == "__main__":
    main()
